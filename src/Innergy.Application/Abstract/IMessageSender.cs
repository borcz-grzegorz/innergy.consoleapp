﻿using Innergy.Application.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Abstract
{
    public interface IMessageSender
    {
        Task Send(string message);
        Task Send(List<Warehouse> warehouses);
    }
}
