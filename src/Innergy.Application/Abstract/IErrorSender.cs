﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Abstract
{
    public interface IErrorSender
    {
        void SendMessage(string message);
    }
}
