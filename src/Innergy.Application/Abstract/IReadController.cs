﻿using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Abstract
{
    public interface IReadController
    {
        Task<ReadContext> ReadAsync();
    }
}
