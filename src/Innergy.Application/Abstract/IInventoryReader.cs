﻿using Innergy.Application.Models;
using System.Collections.Generic;

namespace Innergy.Application.Abstract
{
    public interface IInventoryReader
    {
        IAsyncEnumerable<ReadResult> ReadAsync();
    }
}
