﻿using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Abstract
{
    public interface IWarehouseService
    {
        List<Warehouse> CreateWarehousesSummary(List<InventoryDto> inventories);
    }
}
