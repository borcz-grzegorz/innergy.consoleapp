﻿using System.Text.RegularExpressions;

namespace Innergy.Application.Concrete.Validator
{
    public class StringValidator
    {
        private const string PATTERN = @"^.+;.+;(.+,[1-9][0-9]*\|)*(.+,[1-9][0-9]*)$";
        public static bool IsValid(string value)
        {
            Match match = Regex.Match(value, PATTERN);
            return match.Success;
        }
    }
}
