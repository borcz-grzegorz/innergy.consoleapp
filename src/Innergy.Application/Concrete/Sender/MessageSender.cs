﻿using Innergy.Application.Abstract;
using Innergy.Application.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Concrete.Sender
{
    public class MessageSender : IMessageSender
    {
        private readonly Stream _stream;

        public MessageSender(Stream stream)
        {
            _stream = stream;
        }

        public Task Send(List<Warehouse> warehouses)
        {
            MessageBuilder messageBuilder = new MessageBuilder();
            IEnumerable<Warehouse> orderedWarehouses = warehouses
                                                        .OrderByDescending(x => x.TotalAmount)
                                                        .ThenByDescending(x => x.Name);

            foreach (Warehouse warehouse in orderedWarehouses)
            {
                messageBuilder.AppendWarehouse(warehouse.Name, warehouse.TotalAmount);
                IEnumerable<Material> orderedMaterials = warehouse.Materials.OrderBy(x => x.Id.ToString());
                foreach (Material material in orderedMaterials)
                {
                    messageBuilder.AddMaterial(material.Id, material.TotalAmount);
                }
            }

            return Send(messageBuilder.Build());
        }

        public async Task Send(string message)
        {
            if (!_stream.CanWrite)
            {
                throw new InvalidOperationException("Cannot write to stream");
            }

            StreamWriter writer = new StreamWriter(_stream);  
            await writer.WriteAsync(message);
            await writer.FlushAsync();       
        }
    }
}
