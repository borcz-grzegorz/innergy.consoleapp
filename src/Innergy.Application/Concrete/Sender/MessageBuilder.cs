﻿using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Concrete.Sender
{
    internal class MessageBuilder
    {
        private readonly StringBuilder _stringBuilder;
        private bool _isFirstWarehouse = true;

        public MessageBuilder()
        {
            _stringBuilder = new StringBuilder();
        }

        internal MessageBuilder AppendWarehouse(string name, int totalAmount)
        {
            if (!_isFirstWarehouse)
            {
                _stringBuilder.AppendLine();
            }
            else
            {
                _isFirstWarehouse = false;
            }

            _stringBuilder.AppendLine($"{name} (total {totalAmount})");
            return this;
        }

        internal MessageBuilder AddMaterial(MaterialId id, int totalAmount)
        {
            _stringBuilder.AppendLine($"{id}: {totalAmount}");
            return this;
        }

        public string Build() => _stringBuilder.ToString();
        public override string ToString() => Build();
    }
}
