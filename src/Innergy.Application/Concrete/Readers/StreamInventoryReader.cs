﻿using Innergy.Application.Abstract;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Innergy.Application.Concrete.Readers
{
    public class StreamInventoryReader : IInventoryReader
    {
        private readonly Stream _inputStream;
        private readonly InventoryParser _parser;
        public StreamInventoryReader(Stream inputStream)
        {
            _inputStream = inputStream ?? throw new ArgumentNullException(nameof(inputStream));
            _parser = new InventoryParser();
        }

        public async IAsyncEnumerable<ReadResult> ReadAsync()
        {
            if (!_inputStream.CanRead)
            {
                throw new InvalidOperationException("Cannot read from stream!");
            }

            StreamReader reader = new StreamReader(_inputStream);     
            string line;
            while (!string.IsNullOrEmpty((line = await reader.ReadLineAsync())))
            {
                yield return ProcessLine(line);
            }
            
        }

        private ReadResult ProcessLine(string line)
        {
            string trimedLine = line.Trim();
            if (IsComment(trimedLine))
            {
                return ReadResult.AsComment(trimedLine, line);
            }
            else if (_parser.TryParse(trimedLine, out InventoryDto inventory))
            {
                return ReadResult.AsSuccess(inventory, line);
            }

            return ReadResult.AsInvalid(line);
        }

        private bool IsComment(string line) => line[0] == '#';
    }
}
