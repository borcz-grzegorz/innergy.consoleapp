﻿using Innergy.Application.Abstract;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Concrete.Readers
{
    public class ReadController : IReadController
    {
        private readonly IInventoryReader _reader;
        private readonly IErrorSender _errorSender;

        public ReadController(IInventoryReader reader)
        {
            _reader = reader ?? throw new ArgumentNullException(nameof(reader));
        }

        public ReadController(IInventoryReader reader, IErrorSender errorSender) : this(reader)
        {
            _errorSender = errorSender ?? throw new ArgumentNullException(nameof(errorSender)); ;
        }

        public async Task<ReadContext> ReadAsync()
        {
            List<InventoryDto> inventories = new List<InventoryDto>();
            List<string> errors = new List<string>();

            await foreach (ReadResult readResult in _reader.ReadAsync())
            {
                if (!readResult.IsValid)
                {
                    _errorSender?.SendMessage("Input is not valid!");
                    errors.Add(readResult.OriginInput);
                }
                else if (readResult.Inventory != null)
                {
                    inventories.Add(readResult.Inventory);
                }
            }

            return new ReadContext(inventories, errors);
        }
    }
}
