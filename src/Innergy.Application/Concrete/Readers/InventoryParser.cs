﻿using Innergy.Application.Concrete.Validator;
using Innergy.Application.Models.Dto;
using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Concrete.Readers
{
    public class InventoryParser
    {
        private const short MATERIAL_NAME_POSITION = 0;
        private const short MATERIAL_ID_POSITION = 1;
        private const short WAREHOUSE_LIST_POSITION = 2;
        private const short WAREHOUSE_NAME_POSITION = 0;
        private const short WAREHOUSE_AMMOUNT_POSITION = 1;

        public bool TryParse(string input, out InventoryDto inventory)
        {
            inventory = null;
            input = input ?? throw new ArgumentNullException($"{nameof(input)} is null or empty");
            if (!StringValidator.IsValid(input))
            {
                return false;
            }

            string[] mainParts = input.Split(';');
            inventory = new InventoryDto
            {
                MaterialName = mainParts[MATERIAL_NAME_POSITION],
                MaterialId = new MaterialId(mainParts[MATERIAL_ID_POSITION]),
                WarehouseAmounts = GetWarehouses(mainParts[WAREHOUSE_LIST_POSITION].Split('|'))
            };

            return true;
        }

        private static List<WarehouseAmountDto> GetWarehouses(string[] inputs)
        {
            List<WarehouseAmountDto> warehouses = new List<WarehouseAmountDto>();

            foreach(string input in inputs)
            {
                string[] pair = input.Split(',');
                warehouses.Add(new WarehouseAmountDto
                {
                    WarehouseName = pair[WAREHOUSE_NAME_POSITION],
                    Amount = int.Parse(pair[WAREHOUSE_AMMOUNT_POSITION])
                });
            }

            return warehouses;
        }
    }
}
