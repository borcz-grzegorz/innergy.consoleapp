﻿using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Concrete.Factory
{
    internal class WarehousesFactory
    {
        private readonly List<InventoryDto> _inventories;
        private List<Warehouse> _warehouses;

        internal WarehousesFactory(List<InventoryDto> inventories)
        {
            _inventories = inventories ?? throw new ArgumentNullException(nameof(inventories));
        }

        internal List<Warehouse> Create()
        {
            _warehouses = new List<Warehouse>();

            foreach (InventoryDto inventory in _inventories)
            {
                foreach (WarehouseAmountDto warehouseamount in inventory.WarehouseAmounts)
                {
                    Warehouse warehouse = GetAndCreateIfNotExist(warehouseamount.WarehouseName);
                    warehouse.Add(inventory.MaterialName, inventory.MaterialId, warehouseamount.Amount);
                }
            }

            return _warehouses;
        }

        private Warehouse GetAndCreateIfNotExist(string warehouseName)
        {
            Warehouse warehouse = _warehouses.SingleOrDefault(x => x.Name.ToLower() == warehouseName.ToLower());
            if (warehouse == null)
            {
                warehouse = new Warehouse(warehouseName);
                _warehouses.Add(warehouse);
            }

            return warehouse;
        }
    }
}
