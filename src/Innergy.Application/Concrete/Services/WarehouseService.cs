﻿using Innergy.Application.Abstract;
using Innergy.Application.Concrete.Factory;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Concrete.Services
{
    public class WarehouseService : IWarehouseService
    {
        public List<Warehouse> CreateWarehousesSummary(List<InventoryDto> inventories)
        {
            inventories = inventories ?? throw new ArgumentNullException(nameof(inventories));
            WarehousesFactory factory = new WarehousesFactory(inventories);
            return factory.Create();
        }
    }
}
