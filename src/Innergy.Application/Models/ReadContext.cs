﻿using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Models
{
    public class ReadContext
    {
        public List<string> Errors { get; set; }
        public List<InventoryDto> Inventories { get; set; }

        public ReadContext(List<InventoryDto> inventories, List<string> errors)
        {
            Inventories = inventories;
            Errors = errors;
        }
    }
}
