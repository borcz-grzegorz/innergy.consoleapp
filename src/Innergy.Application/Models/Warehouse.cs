﻿using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innergy.Application.Models
{
    public class Warehouse
    {
        private List<Material> _materials = new List<Material>();
        public string Name { get; }
        public int TotalAmount { get; private set; }
        public IReadOnlyCollection<Material> Materials => _materials.AsReadOnly();

        public Warehouse(string name)
        {
            Name = !string.IsNullOrEmpty(name) ? name : throw new ArgumentException($"{name} is null or empty!");
        }

        internal void Add(string materialName,
                          MaterialId materialId,
                          int amount)
        {
            TotalAmount += amount;
            Material material = GetMaterialAndCreateIfNotExist(materialId, materialName);
            material.Add(amount);
        }

        private Material GetMaterialAndCreateIfNotExist(MaterialId materialId, string materialName)
        {
            Material material = _materials.SingleOrDefault(x => x.Id == materialId);
            if (material == null)
            {
                material = new Material(materialId, materialName);
                _materials.Add(material);
            }

            return material;
        }

        public override string ToString() => $"{nameof(Name)}={Name} {nameof(TotalAmount)}={TotalAmount}";
    }
}
