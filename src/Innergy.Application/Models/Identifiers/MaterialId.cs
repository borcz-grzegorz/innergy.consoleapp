﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Models.Identifiers
{
    public class MaterialId
    {
        private static Random _rand = new Random();
        private readonly string _id;
        public static readonly MaterialId Empty = new MaterialId(string.Empty);

        public MaterialId(string id)
        {
            _id = id;
        }

        public static MaterialId NewId() => new MaterialId($"COM-{_rand.Next(10000,100000)}{(char)_rand.Next(97,122)}");

        public static explicit operator MaterialId(string id)
        {
            return new MaterialId(id);
        }

        public static explicit operator string(MaterialId machineId)
        {
            return machineId == null ? string.Empty : machineId._id;
        }

        public static bool operator ==(MaterialId x, MaterialId y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            return x?._id == y?._id;
        }

        public static bool operator !=(MaterialId x, MaterialId y)
        {
            return !(x == y);
        }

        public override string ToString()
        {
            return _id;
        }

        public override bool Equals(object obj)
        {
            var id = (MaterialId)obj;
            return id != null &&
                   _id.Equals(id._id);
        }

        public override int GetHashCode() => EqualityComparer<string>.Default.GetHashCode(_id);
    }
}
