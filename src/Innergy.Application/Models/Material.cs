﻿using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Models
{
    public class Material
    {
        public MaterialId Id { get; }
        public string Name { get; }
        public int TotalAmount { get; private set; }

        public Material(MaterialId materialId, string name)
        {
            Id = materialId ?? throw new ArgumentNullException(nameof(materialId));
            Name = !string.IsNullOrEmpty(name) ? name : 
                    throw new ArgumentException($"{nameof(name)} is null or empty");
        }

        internal void Add(int amount)
        {
            TotalAmount += amount;
        }

        public override string ToString() 
            => $"{nameof(Name)}={Name} {nameof(TotalAmount)}={TotalAmount} {nameof(Id)}={Id}";
    }
}
