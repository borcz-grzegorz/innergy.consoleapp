﻿using Innergy.Application.Models.Dto;

namespace Innergy.Application.Models
{
    public class ReadResult
    {
        public InventoryDto Inventory { get; }
        public string Comment { get; }
        public string OriginInput { get; }

        public bool IsValid => Inventory != null || Comment != null;

        private ReadResult(string originInput)
        {
            OriginInput = originInput;
        }

        private ReadResult(string comment, string originInput)
        {
            Comment = comment;
            OriginInput = originInput;
        }

        private ReadResult(InventoryDto inventory, string originInput)
        {
            Inventory = inventory;
            OriginInput = originInput;
        }

        internal static ReadResult AsComment(string comment, string originInput)
            => new ReadResult(comment, originInput);

        internal static ReadResult AsInvalid(string originInput) 
            => new ReadResult(originInput);

        internal static ReadResult AsSuccess(InventoryDto inventory, string originInput) 
            => new ReadResult(inventory, originInput);
    }
}
