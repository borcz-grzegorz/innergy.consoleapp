﻿namespace Innergy.Application.Models.Dto
{
    public class WarehouseAmountDto
    {
        public string WarehouseName { get; set; }
        public int Amount { get; set; }
    }
}
