﻿using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Models.Dto
{
    public class InventoryDto
    {
        public string MaterialName { get; set; }
        public MaterialId MaterialId { get; set; }
        public List<WarehouseAmountDto> WarehouseAmounts { get; set; }
    }
}
