﻿using Innergy.Application.Tests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.IntegrationTests
{
    public class IntegrationTests
    {
        [Test]
        public async Task Should_process_message()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                .Empty()
                                .AddRow("# Material inventory initial state as of Jan 01 2018")
                                .AddRow("# New materials")
                                .AddRow("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10")
                                .AddRow("Maple Dovetail Drawerbox;COM-124047;WH-A,15")
                                .AddRow("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2")
                                .AddRow("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11")
                                .AddRow("# Existing materials, restocked")
                                .AddRow("Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5")
                                .AddRow("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3M-Cherry-10mm;WH-A,10|WH-B,1")
                                .AddRow("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10")
                                .AddRow("MDF, CARB2, 1 1/8\";COM-101734;WH-C,8")
                                .Build();

            Stream outputStream = new MemoryStream();

            // act
            await ConsoleApp.Program.Process(inputStream, outputStream);

            // assert
            outputStream.Assert()
                        .LineIsEqualTo("WH-A (total 50)")
                        .LineIsEqualTo("3M-Cherry-10mm: 10")
                        .LineIsEqualTo("COM-100001: 5")
                        .LineIsEqualTo("COM-123906c: 10")
                        .LineIsEqualTo("COM-123908: 10")
                        .LineIsEqualTo("COM-124047: 15")
                        .LineIsEmpty()
                        .LineIsEqualTo("WH-C (total 33)")
                        .LineIsEqualTo("CB0115-CASSRC: 13")
                        .LineIsEqualTo("COM-101734: 8")
                        .LineIsEqualTo("COM-123823: 10")
                        .LineIsEqualTo("COM-123906c: 2")
                        .LineIsEmpty()
                        .LineIsEqualTo("WH-B (total 33)")
                        .LineIsEqualTo("3M-Cherry-10mm: 1")
                        .LineIsEqualTo("CB0115-CASSRC: 5")
                        .LineIsEqualTo("COM-100001: 10")
                        .LineIsEqualTo("COM-123906c: 6")
                        .LineIsEqualTo("COM-123908: 11")
                        .End();

        }
    }
}
