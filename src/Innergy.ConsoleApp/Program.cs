﻿using Innergy.Application.Concrete.Readers;
using Innergy.ConsoleApp.Pipeline;
using Innergy.Pipeline;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Innergy.ConsoleApp
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Stream inputStream = Console.OpenStandardInput();
            Stream outputStream = Console.OpenStandardOutput();

            await Process(inputStream, outputStream);
        }

        public static Task Process(Stream inputStream, Stream outputStream)
        {
            Pipeline<Message> pipeline = new Pipeline<Message>();

            pipeline.Register(() => new ReadFilter(inputStream))
                    .Register(() => new WarehouseFilter())
                    .Register(() => new SendFilter(outputStream));

            Message message = new Message();
            return pipeline.Process(message);
        }
    }
}
