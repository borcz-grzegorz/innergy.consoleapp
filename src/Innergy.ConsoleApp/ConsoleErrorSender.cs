﻿using Innergy.Application.Abstract;
using System;

namespace Innergy.ConsoleApp
{
    public class ConsoleErrorSender : IErrorSender
    {
        public void SendMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
