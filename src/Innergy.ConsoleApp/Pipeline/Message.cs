﻿using Innergy.Application.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.ConsoleApp.Pipeline
{
    public class Message
    {
        public ReadContext ReadContext { get; set; }
        public List<Warehouse> Warehouses { get; set; }
    }
}
