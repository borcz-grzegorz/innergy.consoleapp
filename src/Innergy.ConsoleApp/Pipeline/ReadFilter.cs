﻿using Innergy.Application.Concrete.Readers;
using Innergy.Pipeline;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.ConsoleApp.Pipeline
{
    public class ReadFilter : IFilter<Message>
    {
        private readonly Stream _inputStream;

        public ReadFilter(Stream inputStream)
        {
            _inputStream = inputStream;
        }

        public async Task<Message> Execute(Message message)
        {
            ReadController readController = new ReadController(new StreamInventoryReader(_inputStream),
                                                               new ConsoleErrorSender());

            message.ReadContext = await readController.ReadAsync();

            return message;
        }
    }
}
