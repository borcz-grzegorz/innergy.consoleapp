﻿using Innergy.Application.Concrete.Sender;
using Innergy.Pipeline;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.ConsoleApp.Pipeline
{
    public class SendFilter : IFilter<Message>
    {
        private readonly Stream _stream;

        public SendFilter(Stream outputStream)
        {
            _stream = outputStream;
        }

        public async Task<Message> Execute(Message message)
        {
            MessageSender sender = new MessageSender(_stream);
            await sender.Send(message.Warehouses);
            return message;
        }
    }
}
