﻿using Innergy.Application.Concrete.Services;
using Innergy.Pipeline;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.ConsoleApp.Pipeline
{
    public class WarehouseFilter : IFilter<Message>
    {
        public Task<Message> Execute(Message message)
        {
            WarehouseService service = new WarehouseService();
            message.Warehouses = service.CreateWarehousesSummary(message.ReadContext.Inventories);
            return Task.FromResult(message);
        }
    }
}
