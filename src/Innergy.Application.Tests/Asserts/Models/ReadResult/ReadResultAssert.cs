﻿using Innergy.Application.Models;
using NUnit.Framework;

namespace Innergy.Application.Tests
{
    public class ReadResultAssert
    {
        private readonly ReadResult _actual;

        public ReadResultAssert(ReadResult actual)
        {
            _actual = actual;
        }

        public ReadResultAssert IsValid()
        {
            Assert.True(_actual.IsValid);
            return this;
        }

        public ReadResultAssert IsInValid()
        {
            Assert.False(_actual.IsValid);
            return this;
        }

        public ReadResultAssert IsComment(string expected)
        {
            Assert.NotNull(_actual.Comment);
            Assert.AreEqual(expected, _actual.Comment);
            return this;
        }
    }
}
