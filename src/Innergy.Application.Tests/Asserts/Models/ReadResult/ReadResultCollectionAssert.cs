﻿using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests.Asserts
{
    public class ReadResultCollectionAssert
    {
        private IEnumerator<ReadResult> _enumerator;
        private readonly List<ReadResult> _actual;

        public ReadResultCollectionAssert(IEnumerable<ReadResult> actual)
        {
            _actual = actual.ToList();
        }

        public ReadResultCollectionAssert CountIsEqualTo(int expected)
        {
            Assert.AreEqual(expected, _actual.Count);
            return this;
        }

        public ReadResultCollectionAssert AllAreValid() => Foreach(c => c.Assert().IsValid());

        public ReadResultCollectionAssert AreCommentsEqualTo(params string[] expected)
        => For((i, actual) => actual.Assert().IsComment(expected[i]));

        public ReadResultCollectionAssert BeginWithComment(string expected)
        => Begin(c => c.Assert().IsComment(expected));

        public ReadResultCollectionAssert ThenComment(string expected)
        => Then(c => c.Assert().IsComment(expected));

        public ReadResultCollectionAssert ThenInvalid()
        => Then(c => c.Assert().IsInValid());

        public ReadResultCollectionAssert ThenCollection(List<InventoryDto> firstCollection)
        => ThenMany(firstCollection.Count, (i, c) => c.Inventory.Assert().IsEqualTo(firstCollection[i]));

        public ReadResultCollectionAssert End()
        {
            Assert.False(_enumerator.MoveNext());
            return this;
        }

        private ReadResultCollectionAssert ThenMany(int count, Action<int, ReadResult> action)
        {
            for (int i = 0; i < count; i++)
            {
                action(i, MoveNext());
            }

            return this;
        }

        private ReadResultCollectionAssert Then(Action<ReadResult> action)
        {
            action(MoveNext());
            return this;
        }

        private ReadResultCollectionAssert Begin(Action<ReadResult> action)
        {
            _enumerator = _actual.GetEnumerator();
            action(MoveNext());
            return this;
        }

        private ReadResult MoveNext()
        {
            if (_enumerator.MoveNext())
            {
                return _enumerator.Current;
            }

            Assert.Fail("End of collection!");
            return null;
        }

        private ReadResultCollectionAssert For(Action<int, ReadResult> action)
        {
            for (int i = 0; i < _actual.Count; i++)
            {
                action(i, _actual[i]);
            }

            return this;
        }

        private ReadResultCollectionAssert Foreach(Action<ReadResult> action)
        {
            foreach (ReadResult result in _actual)
            {
                action(result);
            }

            return this;
        }
    }
}
