﻿using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class InventoryDtoAssert
    {
        private readonly InventoryDto _actual;

        public InventoryDtoAssert(InventoryDto actual)
        {
            _actual = actual;
        }

        public InventoryDtoAssert IsEqualTo(InventoryDto expected)
        {
            Assert.AreEqual(expected.MaterialName, _actual.MaterialName);
            Assert.AreEqual(expected.MaterialId, _actual.MaterialId);

            var orderedExpectedWarehouses = expected.WarehouseAmounts.OrderBy(x => x.WarehouseName);
            var orderedActualWarehouses = _actual.WarehouseAmounts.OrderBy(x => x.WarehouseName);

            AreEqual(orderedExpectedWarehouses, orderedActualWarehouses);

            return this;
        }

        private void AreEqual(IOrderedEnumerable<WarehouseAmountDto> expected, 
                              IOrderedEnumerable<WarehouseAmountDto> actual)
        {
            Assert.AreEqual(expected.Count(), actual.Count());

            for (int i = 0; i < expected.Count(); i++)
            {
                AreEqual(expected.ElementAt(i), actual.ElementAt(i));
            }
        }

        private void AreEqual(WarehouseAmountDto expected, WarehouseAmountDto actual)
        {
            Assert.AreEqual(expected.WarehouseName, actual.WarehouseName);
            Assert.AreEqual(expected.Amount, actual.Amount);
        }
    }
}
