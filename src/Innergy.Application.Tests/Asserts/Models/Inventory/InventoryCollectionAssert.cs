﻿using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests.Asserts.Models.Inventory
{
    public class InventoryCollectionAssert
    {
        private List<InventoryDto> _actual;

        public InventoryCollectionAssert(IEnumerable<InventoryDto> actual)
        {
            _actual = actual.ToList();
        }

        public InventoryCollectionAssert IsEmpty()
        {
            CollectionAssert.IsEmpty(_actual);
            return this;
        }

        public InventoryCollectionAssert AreEqual(params List<InventoryDto>[] expected)
        => AreEqual(expected.SelectMany(x => x).ToList());

        public InventoryCollectionAssert AreEqual(List<InventoryDto> expected)
            => Foreach((i, a) => a.Assert().IsEqualTo(expected[i]));

        private InventoryCollectionAssert Foreach(Action<int, InventoryDto> action)
        {
            for (int i = 0; i < _actual.Count; i++)
            {
                action(i, _actual[i]);
            }

            return this;
        }
    }
}
