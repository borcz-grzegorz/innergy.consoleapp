﻿using Innergy.Application.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class WarehouseCollectionAssert
    {
        private readonly List<Warehouse> _actual;

        public WarehouseCollectionAssert(IEnumerable<Warehouse> actual)
        {
            _actual = actual.ToList();
        }

        public WarehouseCollectionAssert CountIsEqualTo(int expected)
        {
            Assert.AreEqual(expected, _actual.Count);
            return this;
        }

        public WarehouseCollectionAssert For(string warehouseName, Action<WarehouseAssert> assertAction)
        {
            Warehouse actual = _actual.Single(x => x.Name.ToLower() == warehouseName);
            assertAction(actual.Assert());
            return this;
        }
    }
}
