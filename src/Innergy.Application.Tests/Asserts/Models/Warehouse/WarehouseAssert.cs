﻿using Innergy.Application.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class WarehouseAssert
    {
        private readonly Warehouse _actual;

        public WarehouseAssert(Warehouse actual)
        {
            _actual = actual;
        }

        public WarehouseAssert TotalAmountIsEqualTo(int expected)
        {
            Assert.AreEqual(expected, _actual.TotalAmount);
            return this;
        }

        public WarehouseAssert HasMaterials(params string[] expectedMaterials)
        {
            List<string> materialNames = _actual.Materials.Select(x => x.Name).ToList();

            foreach (string expected in expectedMaterials)
            {
                Assert.True(materialNames.Contains(expected));
            }

            return this;
        }
    }
}
