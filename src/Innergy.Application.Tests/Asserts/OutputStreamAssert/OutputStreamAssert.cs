﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Innergy.Application.Tests.Asserts
{
    public class OutputStreamAssert
    {
        private IEnumerator<string> _enumerator;
        private readonly List<string> _readLines = new List<string>();

        public OutputStreamAssert(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            using (StreamReader reader = new StreamReader(stream))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    _readLines.Add(line);
                }
            }
        }

        public OutputStreamAssert IsWarehouseLine(string warehouseName, int amount)
        => LineIsEqualTo($"{warehouseName} (total {amount})");

        public OutputStreamAssert SkipLines(int count)
        {
            for (int i = 0; i < count; i++)
            {
                NextLine((line) => { });
            }
            return this;
        }

        public OutputStreamAssert LineIsEmpty() => LineIsEqualTo(string.Empty);

        public OutputStreamAssert LineIsEqualTo(string expected)
            => NextLine(actualLine => Assert.AreEqual(expected, actualLine));

        private OutputStreamAssert NextLine(Action<string> action)
        {
            if (_enumerator == null)
            {
                _enumerator = _readLines.GetEnumerator();
            }
            _enumerator.MoveNext();
            action(_enumerator.Current);
            return this;
        }

        public OutputStreamAssert End()
        {
            Assert.False(_enumerator.MoveNext());
            _enumerator = null;
            return this;
        }
    }
}
