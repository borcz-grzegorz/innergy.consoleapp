﻿using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using Innergy.Application.Tests.Asserts;
using Innergy.Application.Tests.Asserts.Models.Inventory;
using System.Collections.Generic;
using System.IO;

namespace Innergy.Application.Tests
{
    public static class AssertsExtensions
    {
        public static OutputStreamAssert Assert(this Stream stream) => new OutputStreamAssert(stream);
        public static WarehouseCollectionAssert Assert(this IEnumerable<Warehouse> actual) 
            => new WarehouseCollectionAssert(actual);
        public static WarehouseAssert Assert(this Warehouse actual) => new WarehouseAssert(actual);
        public static ReadResultCollectionAssert Assert(this IEnumerable<ReadResult> actual)
            => new ReadResultCollectionAssert(actual);
        public static ReadResultAssert Assert(this ReadResult actual) => new ReadResultAssert(actual);
        public static InventoryCollectionAssert Assert(this IEnumerable<InventoryDto> actual)
            => new InventoryCollectionAssert(actual);
        public static InventoryDtoAssert Assert(this InventoryDto actual) => new InventoryDtoAssert(actual); 
    }
}
