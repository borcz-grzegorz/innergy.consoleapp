﻿using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests
{
    public static class BuilderExtensions
    {
        public static Dictionary<string, MaterialId> CreateDictionary(this IEnumerable<string> source)
        {
            var dictionary = new Dictionary<string, MaterialId>();

            foreach (string item in source)
            {
                dictionary.Add(item, MaterialId.NewId());
            }

            return dictionary;
        }
    }
}
