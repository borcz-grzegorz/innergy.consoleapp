﻿using Innergy.Application.Models.Identifiers;
using RandomDataGenerator.FieldOptions;
using RandomDataGenerator.Randomizers;
using System;

namespace Innergy.Application.Tests
{
    public static class InputMessageBuilderExtensions
    {
        private static Random _rand = new Random();

        public static InputMessageBuilder FromRandom(this InputMessageBuilder builder)
            => builder.RandomName().RandomId().AddManyRandomWarehouseAmount(_rand.Next(1, 10));

        public static InputMessageBuilder RandomName(this InputMessageBuilder builder)
        {
            RandomizerFactory.GetRandomizer(new FieldOptionsText() { UseNumber = false, UseNullValues = false, UseSpace = true });
            builder.MaterialName(WordRandom.GetWord());
            return builder;
        }

        public static InputMessageBuilder AddManyRandomWarehouseAmount(this InputMessageBuilder builder, int count)
        {
            for (int i = 0; i < count; i++)
            {
                builder.AddRandomWarehouseAmount();
            }

            return builder;
        }

        public static InputMessageBuilder AddRandomWarehouseAmount(this InputMessageBuilder builder)
            => builder.AddWarehoueAmount(WordRandom.GetWord(), _rand.Next(1, 100));

        public static InputMessageBuilder RandomId(this InputMessageBuilder builder) => builder.Id(MaterialId.NewId());
        
    }
}
