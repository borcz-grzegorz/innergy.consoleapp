﻿using Innergy.Application.Models.Dto;
using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class InventoryDtoCollectionBuilder
    {
        private static Random _rand = new Random();
        private List<InventoryDto> _inventories = new List<InventoryDto>();

        public static InventoryDtoCollectionBuilder Empty() => new InventoryDtoCollectionBuilder();

        public static InventoryDtoCollectionBuilder FromNames(string[] materialNames,
                                                              string[] warehouseNames,
                                                              int count)
        {
            var builder = new InventoryDtoCollectionBuilder();

            Dictionary<string, MaterialId> dictionary = materialNames.CreateDictionary();
            List<string> warehouseNamesList = warehouseNames.ToList();

            for (int i = 0; i < count; i++)
            {
                string materialName = materialNames.GetRandomItem();
                MaterialId materialId = dictionary[materialName];
                string[] warehouses = warehouseNamesList.GetRandomRange().ToArray();
                builder.Add(materialName, materialId, warehouses);
            }

            return builder;
        }

        private InventoryDtoCollectionBuilder()
        { }

        public List<InventoryDto> Build() => _inventories.ToList();

        public InventoryDtoCollectionBuilder Add(string warehouseName, int totalAmount, params string[] materials)
        {
            int added = 0;

            do
            {
                int count = _rand.Next(1, totalAmount - added);
                string material = materials.GetRandomItem();
                Add(InventoryDtoBuilder
                    .Empty()
                    .MaterialName(material)
                    .Id(new MaterialId(material.GetHashCode().ToString()))
                    .AddWarehouseAmount(warehouseName, count)
                    .Get());
                added += count;
            } while (added < totalAmount);


            return this;
        }

        public InventoryDtoCollectionBuilder AddMany(int count) => For(count, () => Add());
        public InventoryDtoCollectionBuilder Add(string materialName, string materialId, params string[] warehouseName)
            => Add(materialName, (MaterialId)materialId, warehouseName);
        public InventoryDtoCollectionBuilder Add(string materialName, MaterialId materialId, params string[] warehouseName)
            => Add(InventoryDtoBuilder.From(materialName, materialId, warehouseName).Get());
        public InventoryDtoCollectionBuilder Add(string materialName, params string[] warehouseName)
            => Add(InventoryDtoBuilder.From(materialName, MaterialId.NewId(), warehouseName).Get());
        public InventoryDtoCollectionBuilder Add() => Add(InventoryDtoBuilder.Filled().Get());
        public InventoryDtoCollectionBuilder Add(InventoryDto dto)
        {
            _inventories.Add(dto);
            return this;
        }

        private InventoryDtoCollectionBuilder For(int count, Action action) => For(count, (i, b) => action());
        private InventoryDtoCollectionBuilder For(int count, Action<int> action) => For(count, (i, b) => action(i));
        private InventoryDtoCollectionBuilder For(int count, Action<int, InventoryDtoCollectionBuilder> action)
        {
            for (int i = 0; i < count; i++)
            {
                action(i, this);
            }
            return this;
        }
    }
}
