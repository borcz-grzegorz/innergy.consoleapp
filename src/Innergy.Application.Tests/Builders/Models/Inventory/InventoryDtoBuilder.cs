﻿using Innergy.Application.Models.Dto;
using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests
{
    public class InventoryDtoBuilder
    {
        private static Random _rand = new Random();
        private InventoryDto _dto = new InventoryDto
        {
            WarehouseAmounts = new List<WarehouseAmountDto>()
        };

        public static InventoryDtoBuilder Empty() => new InventoryDtoBuilder();
        public static InventoryDtoBuilder Filled()
            => Empty()
                .MaterialName(WordRandom.GetWord())
                .Id(MaterialId.NewId())
                .AddRandomWarehouses(WordRandom.GetWord(), WordRandom.GetWord(), WordRandom.GetWord());
        public static InventoryDtoBuilder From(string materialName, MaterialId materialId, params string[] warehouseName)
            => Empty()
                .MaterialName(materialName)
                .Id(materialId)
                .AddRandomWarehouses(warehouseName);

        private InventoryDtoBuilder()
        {}

        public InventoryDtoBuilder MaterialName(string materialName)
        {
            _dto.MaterialName = materialName;
            return this;
        }

        public InventoryDtoBuilder Id(MaterialId materialId)
        {
            _dto.MaterialId = materialId;
            return this;
        }

        public InventoryDtoBuilder AddRandomWarehouses(params string[] warehousesNames)
        {
            foreach(string warehouseName in warehousesNames)
            {
                AddWarehouseAmount(warehouseName, _rand.Next(1, 20));
            }

            return this;
        }

        public InventoryDtoBuilder AddWarehouseAmount(string name, int amount)
        => AddWarehouseAmount(new WarehouseAmountDto
        {
            WarehouseName = name,
            Amount = amount
        });

        public InventoryDtoBuilder AddWarehouseAmount(WarehouseAmountDto dto)
        {
            _dto.WarehouseAmounts.Add(dto);
            return this;
        }

        public InventoryDto Get() => _dto;
    }
}
