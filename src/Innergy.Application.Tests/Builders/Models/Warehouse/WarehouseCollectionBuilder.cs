﻿using Innergy.Application.Models;
using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class WarehouseCollectionBuilder
    {
        private List<Warehouse> _warehouses = new List<Warehouse>();

        private WarehouseCollectionBuilder()
        { }

        public static WarehouseCollectionBuilder Empty() => new WarehouseCollectionBuilder();

        public List<Warehouse> Build() => _warehouses.ToList();

        public WarehouseCollectionBuilder AddMaterialToLastWarehouse(string materialId, int amount)
            => AddMaterialToLastWarehouse(materialId.GetHashCode().ToString(), new MaterialId(materialId), amount);

        public WarehouseCollectionBuilder AddMaterialToLastWarehouse(string materialName, MaterialId materialId, int amount)
        {
            Warehouse warehouse = _warehouses.Last();
            warehouse.Add(materialName, materialId, amount);
            return this;
        }

        public WarehouseCollectionBuilder AddWarehouse(string name)
            => Add(new Warehouse(name));

        public WarehouseCollectionBuilder Add(Warehouse warehouse)
        {
            _warehouses.Add(warehouse);
            return this;
        }
    }
}
