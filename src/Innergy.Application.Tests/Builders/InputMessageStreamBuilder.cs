﻿using Innergy.Application.Models.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Innergy.Application.Tests
{
    public class InputMessageStreamBuilder
    {
        private readonly List<string> _rows = new List<string>();

        public static InputMessageStreamBuilder Empty() => new InputMessageStreamBuilder();

        private InputMessageStreamBuilder()
        { }

        public Stream Build()
        {
            MemoryStream memoryStream = new MemoryStream();
            StreamWriter writer = new StreamWriter(memoryStream);

            foreach (string row in _rows)
            {
                writer.WriteLine(row);
            }

            writer.Flush();
            memoryStream.Seek(0, SeekOrigin.Begin);

            StreamReader reader = new StreamReader(memoryStream);
            return memoryStream;
        }

        public InputMessageStreamBuilder AddManyComments(int count) => For(count, () => AddComment());
        public InputMessageStreamBuilder AddComment() => AddComment("# Standard comment");
        public InputMessageStreamBuilder AddComment(string commentMessage) => AddRow($"# {commentMessage}");

        public InputMessageStreamBuilder FromInventories(IEnumerable<InventoryDto> inventories)
            => For(inventories.Count(), (i) => AddFromInventory(inventories.ElementAt(i)));

        public InputMessageStreamBuilder AddManyFromInventories(int count, params InventoryDto[] inventories)
            => For(count, () => AddFromInventory(inventories.GetRandomItem()));
        public InputMessageStreamBuilder AddManyValidRows(int count) => For(count, () => AddValidRow());
        public InputMessageStreamBuilder AddFromInventory(InventoryDto inventory) => AddRow(InputMessageBuilder.From(inventory).Build());
        public InputMessageStreamBuilder AddValidRow() => AddRow(InputMessageBuilder.Empty().FromRandom().Build());
        public InputMessageStreamBuilder AddEmptyRow() => AddRow(string.Empty);
        public InputMessageStreamBuilder AddRow(string message)
        {
            _rows.Add(message);
            return this;
        }

        public InputMessageStreamBuilder AddManyInventories(int count, out List<InventoryDto> expected)
        {
            expected = new List<InventoryDto>();
            for (int i = 0; i < count; i++)
            {
                AddInventory(out InventoryDto inventory);
                expected.Add(inventory);
            }

            return this;
        }

        public InputMessageStreamBuilder AddInventory() => AddInventory(out InventoryDto i);

        public InputMessageStreamBuilder AddInventory(out InventoryDto inventory)
            => AddFromInventory(inventory = InventoryDtoBuilder.Filled().Get());

        private InputMessageStreamBuilder For(int count, Action action) => For(count, (i, b) => action());
        private InputMessageStreamBuilder For(int count, Action<int> action) => For(count, (i, b) => action(i));
        private InputMessageStreamBuilder For(int count, Action<int, InputMessageStreamBuilder> action)
        {
            for (int i = 0; i < count; i++)
            {
                action(i, this);
            }

            return this;
        }
    }
}
