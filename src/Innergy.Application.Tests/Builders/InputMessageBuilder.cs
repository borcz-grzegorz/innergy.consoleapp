﻿using Innergy.Application.Models.Dto;
using Innergy.Application.Models.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public class InputMessageBuilder
    {
        private string _materialName;
        private MaterialId _id;
        private List<(string warehouseName, int amount)> _warehouseCountPairs = new List<(string warehouseName, int amount)>();

        public static InputMessageBuilder Empty() => new InputMessageBuilder();

        public static InputMessageBuilder From(InventoryDto inventory)
            => Empty()
               .MaterialName(inventory.MaterialName)
               .Id(inventory.MaterialId)
               .AddWarehoueAmount(inventory.WarehouseAmounts);

        private InputMessageBuilder()
        { }

        public string Build()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(_materialName);
            builder.Append(';');
            builder.Append((string)_id);
            
            if (!_warehouseCountPairs.Any())
            {
                return builder.ToString();
            }

            builder.Append(';');
            for (int i = 0; i < _warehouseCountPairs.Count - 1; i++)
            {
                var pair = _warehouseCountPairs[i];
                AppendWarehouseAmount(builder, pair);
                builder.Append('|');
            }

            AppendWarehouseAmount(builder, _warehouseCountPairs.Last());
            return builder.ToString();
        }

        private static void AppendWarehouseAmount(StringBuilder builder, (string warehouseName, int amount) pair)
        {
            builder.Append(pair.warehouseName);
            builder.Append(',');
            builder.Append(pair.amount);
        }

        public InputMessageBuilder MaterialName(string materialName)
        {
            _materialName = materialName ?? string.Empty;
            return this;
        }

        public InputMessageBuilder Id(MaterialId id)
        {
            _id = id;
            return this;
        }

        public InputMessageBuilder AddWarehoueAmount(List<WarehouseAmountDto> warehouses)
        {
            foreach(WarehouseAmountDto warehouse in warehouses)
            {
                AddWarehoueAmount(warehouse);
            }

            return this;
        }

        public InputMessageBuilder AddWarehoueAmount(WarehouseAmountDto dto)
            => AddWarehoueAmount(dto.WarehouseName, dto.Amount);

        public InputMessageBuilder AddWarehoueAmount(string warehouseName, int amount)
        {
            _warehouseCountPairs.Add((warehouseName, amount));
            return this;
        }

        public override string ToString() => Build();

    }
}
