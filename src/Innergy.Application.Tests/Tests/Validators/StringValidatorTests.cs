﻿using Innergy.Application.Concrete.Validator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests.Tests.Validators
{
    public class StringValidatorTests
    {
        [Test]
        public void Should_validate_correct_input()
        {
            // act
            bool isValid = StringValidator.IsValid("Material;Id;space,67|space,67|space,67");

            // assert
            Assert.True(isValid);
        }

        [TestCase(";Id;space,67|space,67|space,67")]
        [TestCase("Material;;space,67|space,67|space,67")]
        [TestCase("Material_Id;space,67|space,67|space,67")]
        [TestCase("Material;Id;space,67|space,67|space,67|")]
        [TestCase("Material;Id;|")]
        [TestCase("Material;Id;space,67|space,67|space,6-7")]
        [TestCase("Material;Id;space,67|space,67|space,67 ")]
        [TestCase("Material;Id;space,67?")]
        public void Should_return_false_when_input_is_not_valid(string input)
        {
            // act
            bool isValid = StringValidator.IsValid(input);

            // assert
            Assert.False(isValid);
        }

    }
}
