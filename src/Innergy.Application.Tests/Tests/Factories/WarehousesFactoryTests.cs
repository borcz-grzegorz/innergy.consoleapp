﻿using Innergy.Application.Concrete.Factory;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests.Tests.Factories
{
    public class WarehousesFactoryTests
    {
        [Test]
        public void Should_throw_argument_null_exception_when_collection_is_null()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(() => new WarehousesFactory(null));
        }

        [Test]
        public void Should_correctly_set_total_amount()
        {
            // arrange
            List<InventoryDto> inventories = InventoryDtoCollectionBuilder
                                             .Empty()
                                             .Add("warehouse1", 88, "Material1", "Material2", "Material3")
                                             .Add("warehouse2", 130, "Material3", "Material4")
                                             .Add("warehouse3", 17, "Material1", "Material2")
                                             .Build();

            var factory = new WarehousesFactory(inventories);

            // act
            List<Warehouse> warehouses = factory.Create();

            // assert
            warehouses.Assert()
                      .CountIsEqualTo(expected: 3)
                      .For("warehouse1", a => a.TotalAmountIsEqualTo(88))
                      .For("warehouse2", a => a.TotalAmountIsEqualTo(130))
                      .For("warehouse3", a => a.TotalAmountIsEqualTo(17));
        }

        [Test]
        public void Should_correctly_assign_materials_to_warehouses()
        {
            // arrange
            List<InventoryDto> inventories = InventoryDtoCollectionBuilder
                                            .Empty()
                                            .Add("Material1", "Id1", "warehouse1", "warehouse2", "warehouse3")
                                            .Add("Material2", "Id2", "warehouse1", "warehouse2", "warehouse3", "warehouse3")
                                            .Add("Material3", "Id3", "warehouse1", "warehouse3")
                                            .Add("Material1", "Id1", "warehouse1")
                                            .Add("Material2", "Id2", "warehouse2", "warehouse2", "warehouse3")
                                            .Build();

            var factory = new WarehousesFactory(inventories);

            // act
            List<Warehouse> warehouses = factory.Create();

            // assert
            warehouses.Assert()
                      .CountIsEqualTo(expected: 3)
                      .For("warehouse1", a => a.HasMaterials("Material1", "Material2", "Material3"))
                      .For("warehouse2", a => a.HasMaterials("Material1", "Material2"))
                      .For("warehouse3", a => a.HasMaterials("Material1", "Material2", "Material3"));
        }
    }
}
