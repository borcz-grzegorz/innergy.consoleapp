﻿using Innergy.Application.Abstract;
using Innergy.Application.Concrete.Readers;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Tests.Tests.Readers
{
    public class ReaderControllerTests
    {
        private Mock<IErrorSender> _senderMock;

        [SetUp]
        public void SetUp()
        {
            _senderMock = new Mock<IErrorSender>();
        }

        [Test]
        public void Should_throw_argument_null_exception_when_reader_is_null()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(() => new ReadController(null));
        }

        [Test]
        public void Should_throw_argument_null_exception_when_error_sender_is_null()
        {
            // arrange 
            Stream inputStream = InputMessageStreamBuilder.Empty().Build();
            var reader = new StreamInventoryReader(inputStream);

            // act & assert
            Assert.Throws<ArgumentNullException>(() => new ReadController(reader, null));
        }

        [Test]
        public async Task Should_send_error_messages()
        {
            // arrange 
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddComment("Comment")
                                    .AddValidRow()
                                    .AddComment()
                                    .AddRow("Some invalid message")
                                    .Build();

            _senderMock.Setup(x => x.SendMessage(It.IsAny<string>())).Verifiable();

            // act
            await inputStream.CreateController(_senderMock.Object).ReadAsync();

            // setUp
            _senderMock.Verify();
        }

        [Test]
        public async Task Should_return_inventories()
        {
            // arrange 
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddManyInventories(count: 20, out List<InventoryDto> expected)
                                    .Build();

            // act
            ReadContext result = await inputStream.CreateController().ReadAsync();

            // assert
            result.Inventories
                  .Assert()
                  .AreEqual(expected);
        }

        [Test]
        public async Task Should_return_empty_collection_when_message_contains_only_comments()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddComment("Comment1")
                                    .AddComment("Comment2")
                                    .AddComment("Comment3")
                                    .Build();

            // act
            ReadContext result = await inputStream.CreateController().ReadAsync();

            // assert
            result.Inventories.Assert().IsEmpty();
        }

        [Test]
        public async Task Should_return_errors_and_models()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddComment("Comment1")
                                    .AddManyInventories(10, out List<InventoryDto> firstExpectedInventories)
                                    .AddComment("Comment2")
                                    .AddRow("invalid row")
                                    .AddRow("another invalid row")
                                    .AddManyInventories(20, out List<InventoryDto> secondExpectedInventories)
                                    .AddRow("and one more invalid row! :)")
                                    .AddComment("Comment3")
                                    .Build();

            // act
            ReadContext result = await inputStream.CreateController().ReadAsync();

            // assert
            Assert.AreEqual(result.Errors.Count, 3);
            result.Inventories
                  .Assert()
                  .AreEqual(firstExpectedInventories, secondExpectedInventories);
        }

    }
}
