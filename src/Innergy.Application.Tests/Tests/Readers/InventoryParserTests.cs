﻿using Innergy.Application.Concrete.Readers;
using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests.Readers
{
    public class InventoryParserTests
    {
        [Test]
        public void Should_parse_valid_input()
        {
            // arrange
            InventoryDto expected = InventoryDtoBuilder.Filled().Get();
            string input = InputMessageBuilder.From(expected).Build();
            InventoryParser parser = new InventoryParser();

            // act
            bool result = parser.TryParse(input, out InventoryDto actual);

            // assert
            Assert.True(result);
            actual.Assert()
                  .IsEqualTo(expected);
        }

        [Test]
        public void Should_return_false_when_input_is_incorrect()
        {
            // arrange
            InventoryParser parser = new InventoryParser();

            // act
            bool result = parser.TryParse("test;test\\test", out InventoryDto inventory);

            // assert
            Assert.False(result);
            Assert.Null(inventory);
        }

        [Test]
        public void Should_throw_argument_null_exception_when_input_is_null()
        {
            // arrange
            InventoryParser parser = new InventoryParser();

            // act & assert
            Assert.Throws<ArgumentNullException>(() => parser.TryParse(null, out InventoryDto inventory));
        }
    }
}
