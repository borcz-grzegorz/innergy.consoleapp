﻿using Innergy.Application.Concrete.Readers;
using Innergy.Application.Models;
using Innergy.Application.Models.Dto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Innergy.Application.Tests.Tests.Readers
{
    public class StreamInventoryReaderTests
    {
        private StreamInventoryReader _reader;

        [Test]
        public void Should_throw_argument_null_exception_when_stream_is_null()
        {
            // act & assert
            Assert.Throws<ArgumentNullException>(() => new StreamInventoryReader(null));
        }

        [Test]
        public async Task Should_return_empty_list_when_stream_is_empty()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder.Empty().Build();
            _reader = new StreamInventoryReader(inputStream);

            // act 
            List<ReadResult> result = await ReadAll();

            // assert
            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public async Task Should_return_empty_list_when_stream_contains_only_empty_strings()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddEmptyRow()
                                    .AddEmptyRow()
                                    .Build();

            _reader = new StreamInventoryReader(inputStream);

            // act & assert
            List<ReadResult> result = await ReadAll();

            // assert
            CollectionAssert.IsEmpty(result);
        }

        [Test]
        public async Task Should_read_when_stream_contains_only_valid_rows()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddManyInventories(count: 10, out List<InventoryDto> expected)
                                    .Build();

            _reader = new StreamInventoryReader(inputStream);

            // act & assert
            List<ReadResult> result = await ReadAll();

            // assert
            result
            .Assert()
            .CountIsEqualTo(10)
            .AllAreValid();
        }

        [Test]
        public async Task Should_return_only_comments()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddComment("First comment")
                                    .AddComment("Second comment")
                                    .AddComment("third comment")
                                    .Build();
            _reader = new StreamInventoryReader(inputStream);

            // act & assert
            List<ReadResult> result = await ReadAll();

            // assert
            result
                .Assert()
                .CountIsEqualTo(3)
                .AreCommentsEqualTo("# First comment", "# Second comment", "# third comment");
        }

        [Test]
        public async Task Should_return_comments_read_values_and_invalid_inputs()
        {
            // arrange
            Stream inputStream = InputMessageStreamBuilder
                                    .Empty()
                                    .AddComment("First comment")
                                    .AddManyInventories(10, out List<InventoryDto> firstCollection)
                                    .AddComment("Second comment")
                                    .AddRow("some invalid message")
                                    .AddManyInventories(15, out List<InventoryDto> secondCollection)
                                    .Build();

            _reader = new StreamInventoryReader(inputStream);

            // act
            List<ReadResult> result = await ReadAll();

            // assert
            result.Assert()
                  .BeginWithComment("# First comment")
                  .ThenCollection(firstCollection)
                  .ThenComment("# Second comment")
                  .ThenInvalid()
                  .ThenCollection(secondCollection)
                  .End();
        }

        private async Task<List<ReadResult>> ReadAll()
        {
            List<ReadResult> result = new List<ReadResult>();
            await foreach (ReadResult readResult in _reader.ReadAsync())
            {
                result.Add(readResult);
            }

            return result;
        }
    }
}
