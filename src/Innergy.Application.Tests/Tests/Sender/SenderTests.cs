﻿using Innergy.Application.Concrete.Sender;
using Innergy.Application.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Application.Tests.Tests.Sender
{
    public class SenderTests
    {
        private MemoryStream _stream;

        [SetUp]
        public void SetUp()
        {
            _stream = new MemoryStream();
        }

        [TearDown]
        public void TearDown()
        {
            _stream.Dispose();
        }

        [Test]
        public async Task Should_correctly_create_output_message()
        {
            // arrange
            List<Warehouse> warehouses = WarehouseCollectionBuilder
                                         .Empty()
                                         .AddWarehouse("CWarehouse")
                                            .AddMaterialToLastWarehouse("CMaterial", 50)
                                            .AddMaterialToLastWarehouse("BMaterial", 10)
                                            .AddMaterialToLastWarehouse("AMaterial", 1)
                                        .AddWarehouse("BWarehouse")
                                            .AddMaterialToLastWarehouse("AMaterial", 17)
                                            .AddMaterialToLastWarehouse("BMaterial", 20)
                                        .Build();

            MessageSender sender = new MessageSender(_stream);

            // act
            await sender.Send(warehouses);

            // assert
            _stream.Assert()
                   .LineIsEqualTo("CWarehouse (total 61)")
                        .LineIsEqualTo("AMaterial: 1")
                        .LineIsEqualTo("BMaterial: 10")
                        .LineIsEqualTo("CMaterial: 50")
                   .LineIsEmpty()
                   .LineIsEqualTo("BWarehouse (total 37)")
                       .LineIsEqualTo("AMaterial: 17")
                       .LineIsEqualTo("BMaterial: 20")
                   .End();
        }

        [Test]
        public async Task Should_sort_warehouses_when_total_amount_has_same_value()
        {
            // arrange
            List<Warehouse> warehouses = WarehouseCollectionBuilder
                                         .Empty()
                                         .AddWarehouse("BWarehouse")
                                            .AddMaterialToLastWarehouse("CMaterial", 50)
                                            .AddMaterialToLastWarehouse("BMaterial", 50)
                                        .AddWarehouse("CWarehouse")
                                            .AddMaterialToLastWarehouse("AMaterial", 70)
                                            .AddMaterialToLastWarehouse("BMaterial", 30)
                                        .Build();

            MessageSender sender = new MessageSender(_stream);

            // act
            await sender.Send(warehouses);

            // assert
            _stream.Assert()
                   .IsWarehouseLine("CWarehouse", 100)
                   .SkipLines(count: 3)
                   .IsWarehouseLine("BWarehouse", 100);
        }
    }
}
