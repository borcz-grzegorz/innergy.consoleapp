﻿using RandomDataGenerator.FieldOptions;
using RandomDataGenerator.Randomizers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Innergy.Application.Tests
{
    public static class WordRandom
    {
        private static IRandomizerString _wordRandomizer = RandomizerFactory.GetRandomizer(new FieldOptionsText()
        {
            UseNumber = false,
            UseNullValues = false,
            UseSpace = true,
            UseSpecial = false
        });

        public static string GetWord() => _wordRandomizer.Generate();
    }
}
