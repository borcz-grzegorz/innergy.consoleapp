﻿using Innergy.Application.Abstract;
using Innergy.Application.Concrete.Readers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Innergy.Application.Tests
{
    public static class Extensions
    {
        private static Random _rand = new Random();
        public static T GetRandomItem<T>(this IEnumerable<T> source) => source.ElementAt(_rand.Next(0, source.Count()));

        public static List<T> GetRandomRange<T>(this List<T> source)
        {
            int startIndex = _rand.Next(0, source.Count - 1);
            int count = _rand.Next(1, source.Count + 1 - startIndex);
            return source.GetRange(startIndex, count);
        }

        public static ReadController CreateController(this Stream inputStream, IErrorSender sender)
            => new ReadController(new StreamInventoryReader(inputStream), sender);

        public static ReadController CreateController(this Stream inputStream)
            => new ReadController(new StreamInventoryReader(inputStream));
    }
}
