﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Pipeline
{
    public interface IFilter<T>
    {
        Task<T> Execute(T message); 
    }
}
