﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Innergy.Pipeline
{
    public class Pipeline<T>
    {
        private readonly List<Func<IFilter<T>>> _filtersFactories = new List<Func<IFilter<T>>>();

        public Pipeline<T> Register(Func<IFilter<T>> filterFactory)
        {
            _filtersFactories.Add(filterFactory);
            return this;
        }

        public async Task<T> Process(T message)
        {
            foreach (Func<IFilter<T>> filterFactory in _filtersFactories)
            {
                IFilter<T> filter = filterFactory();
                message = await filter.Execute(message);
            }

            return message;
        }
    }
}
